class JobApplicationsController < ApplicationController
  before_action :set_job_application, only: [:show, :update, :destroy]
  #before_action :authenticate_user!, except: [:show, :index]

  def index
    @jobApplcations = JobApplication.all
  end

  def show
  end

  def create
    @job_applcation = JobApplication.new(job_application_params)
    @job_applcation.user_id = current_user.id # current_user.id
    if @job_applcation.save
        render json: @job_applcation, status: :created
      else
        render json: @job_applcation.errors, status: :unprocessable_entity
    end
  end

  def update
    @job_application.update(job_applications_update_params)
    render json: @job_application
  end

  private

  def job_applications_update_params
    params.require(:job_application).permit(:status)
  end

  def job_application_params
    params.require(:job_application).permit(:job_id)
  end

  def set_job_application
    @job_application = JobApplication.find(params[:id])
  end
end
