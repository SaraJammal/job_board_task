class JobsController < ApplicationController
  before_action :set_job, only: [:show, :edit, :update, :destroy]
  #before_action :authenticate_user!, except: [:show, :index]
  # GET /jobs
  # GET /jobs.json


  def index
    @jobs = Job.all
    render json: @jobs

    @jobss = Job.where(nil)
      filtering(params).each do |key, value|
        @jobss = @jobss.public_send("filter_by_#{key}", value) if value.present?
      end
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  def new
    @job = Job.new
  end

  # GET /jobs/1/edit
  def edit
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)
    @job.user = current_user
     if @job.save
        render json: @job, status: :created, location: @job
     else
        render json: @job.errors, status: :unprocessable_entity
     end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
      @job.update(job_params)
      render json: @job, status: :ok, location: @job
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
#      format.json { head :no_content }
  end

  private

  def filtering(params)
    params.slice(:title, :body)
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Only allow a list of trusted parameters through.

   def job_params
      params.require(:job).permit(:title, :body)
   end

end
