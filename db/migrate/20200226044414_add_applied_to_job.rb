class AddAppliedToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :applied, :boolean, default: false
  end
end
