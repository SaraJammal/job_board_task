require 'test_helper'
require "test/unit"

class UserTest < ActiveSupport::TestCase


  # test "the truth" do
  #   assert true
  # end
  test 'valid user' do
    user = User.new(name: 'Sara', email: 'sara@gmail.com')
    assert user.valid?
  end

  test 'invalid without email' do
    user = User.new(name: 'Sara')
    refute user.valid?
    assert_not_nit user.errors[:email]

  end

end
